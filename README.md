# 网络通信-客户端-服务器端

#### 基于TCP协议的C/S模型

`int listen(int sockfd, int backlog);`

**功能**：设置等待连接的最大数量

**sockfd**：被监听的socket描述符

**backlog**：等待连接的最大数量（排队的数量）

**返回值**：成功返回0，失败返回-1

`int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);`

**功能**：等待连接sockfd连接

**addr**：获取连接的地址

**addrlen**：设置连接地址结构体长度

**返回值**：专门用于通信的描述符


**编程模型**：

Server|操作|Client|操作 
|--|--|--|--|
创建socket套接字||创建socket套接字
准备地址|sockaddr_in，本机地址|准备地址|服务器地址
|绑定|bind|
|监听|listen|
|等待连接|accept、fork|连接|connect
|接收请求|read/recv|发送请求|write/send
|响应请求|write/send|接收响应|read/recv
|关闭|close|关闭|close


#### 使用说明

通过阿里云服务器，建立网络通信，可以让服务器端和客户端建立通信关系

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)